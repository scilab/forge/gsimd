/*
 * error.h
 *
 *  Created on: Jun 7, 2010
 *      Author: vlj
 */

#ifndef ERROR_CUDA_HPP_
#define ERROR_CUDA_HPP_

#include <exception>
#include <iostream>
using namespace std;


template<>
inline
void Error::treat_error<CUDAmode>(CUDAmode::Status id)
{
	switch ( id )
	{
	case CUDA_SUCCESS:
		return;
	case CUDA_ERROR_INVALID_VALUE:
		throw Error("CUDA_ERROR_INVALID_VALUE");
	case CUDA_ERROR_OUT_OF_MEMORY:
		throw Error("CUDA_ERROR_OUT_OF_MEMORY");
	case CUDA_ERROR_NOT_INITIALIZED:
		throw Error("CUDA_ERROR_NOT_INITIALIZED");
	case CUDA_ERROR_DEINITIALIZED:
		throw Error("CUDA_ERROR_DEINITIALIZED");
	case CUDA_ERROR_NO_DEVICE:
		throw Error("CUDA_ERROR_NO_DEVICE");
	case CUDA_ERROR_INVALID_DEVICE:
		throw Error("CUDA_ERROR_INVALID_DEVICE");
	case CUDA_ERROR_INVALID_IMAGE:
		throw Error( "CUDA_ERROR_INVALID_IMAGE");
	case CUDA_ERROR_INVALID_CONTEXT:
		throw Error("CUDA_ERROR_INVALID_CONTEXT");
	case CUDA_ERROR_CONTEXT_ALREADY_CURRENT:
		throw Error("CUDA_ERROR_CONTEXT_ALREADY_CURRENT");
	case CUDA_ERROR_MAP_FAILED:
		throw Error("CUDA_ERROR_MAP_FAILED");
	case CUDA_ERROR_UNMAP_FAILED:
		throw Error("CUDA_ERROR_UNMAP_FAILED");
	case CUDA_ERROR_ARRAY_IS_MAPPED:
		throw Error("CUDA_ERROR_ARRAY_IS_MAPPED");
	case CUDA_ERROR_ALREADY_MAPPED:
		throw Error("CUDA_ERROR_ALREADY_MAPPED");
	case CUDA_ERROR_NOT_MAPPED:
		throw Error("CUDA_ERROR_NOT_MAPPED");
	case CUDA_ERROR_NOT_MAPPED_AS_ARRAY:
		throw Error("CUDA_ERROR_NOT_MAPPED_AS_ARRAY");
	case CUDA_ERROR_NOT_MAPPED_AS_POINTER:
		throw Error("CUDA_ERROR_NOT_MAPPED_AS_POINTER");
	case CUDA_ERROR_NO_BINARY_FOR_GPU:
		throw Error("CUDA_ERROR_NO_BINARY_FOR_GPU");
	case CUDA_ERROR_ALREADY_ACQUIRED:
		throw Error("CUDA_ERROR_ALREADY_ACQUIRED");
	case CUDA_ERROR_ECC_UNCORRECTABLE:
		throw Error("CUDA_ERROR_ECC_UNCORRECTABLE");
	case CUDA_ERROR_INVALID_SOURCE:
		throw Error("CUDA_ERROR_INVALID_SOURCE");
	case CUDA_ERROR_FILE_NOT_FOUND:
		throw Error("CUDA_ERROR_FILE_NOT_FOUND");
	case CUDA_ERROR_INVALID_HANDLE:
		throw Error("CUDA_ERROR_INVALID_HANDLE");
	case CUDA_ERROR_NOT_FOUND:
		throw Error( "CUDA_ERROR_NOT_FOUND");
	case CUDA_ERROR_NOT_READY:
		throw Error( "CUDA_ERROR_NOT_READY");
	case CUDA_ERROR_LAUNCH_FAILED:
		throw Error( "CUDA_ERROR_LAUNCH_FAILED");
	case CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES:
		throw Error( "CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES");
	case CUDA_ERROR_LAUNCH_TIMEOUT:
		throw Error( "CUDA_ERROR_LAUNCH_TIMEOUT");
	case CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING:
		throw Error( "CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING");
#ifdef  CUDA_ERROR_POINTER_IS_64BIT
	case CUDA_ERROR_POINTER_IS_64BIT:
		throw Error( "CUDA_ERROR_POINTER_IS_64BIT");
#endif
#ifdef CUDA_ERROR_SIZE_IS_64BIT
	case CUDA_ERROR_SIZE_IS_64BIT:
		throw Error( "CUDA_ERROR_SIZE_IS_64BIT");
#endif
	case CUDA_ERROR_UNKNOWN:
		throw Error("CUDA_ERROR_UNKNOWN");
	default:
		throw Error("");
	}

}


#endif /* ERROR_H_ */
