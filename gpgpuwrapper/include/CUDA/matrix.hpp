#ifndef MATRIX_CUDA_H_
#define MATRIX_CUDA_H_

template<typename T>
class Matrix<CUDAmode, T> : public MatrixBase<T>
{
	friend class Kernel<CUDAmode> ;
	friend class MatrixFactory<CUDAmode, T> ;
	typedef typename CUDAmode::Status Status;
	typedef typename CUDAmode::DevicePtr_Handle DevicePtr_Handle;
	typedef typename CUDAmode::Context_Handle Context_Handle;
	typedef typename CUDAmode::Stream Stream;
	typedef MatrixBase<T> Base;

protected:
	Stream stream;
	Context_Handle cont;
	DevicePtr_Handle ptr;

public:
	inline
	Matrix(CUDAmode::Context_Handle, CUDAmode::Stream, int n, T* tmp) : Base(n)
	{
		__check_sanity__<CUDAmode> (
				cuMemAlloc(&ptr, (Base::length) * sizeof(T)));
		if (tmp != NULL)
		{
			__check_sanity__<CUDAmode> (cuMemcpyHtoD(ptr, tmp, (Base::length)
					* sizeof(T)));
		}
	}

	inline Matrix()
	{

	}

	inline Matrix(const Matrix& other_matrix)
	{
		Base::length = other_matrix.length;
		__check_sanity__<CUDAmode> (cuMemAlloc(&ptr, Base::length * sizeof(T)));
		__check_sanity__<CUDAmode> (cuMemcpyDtoD(ptr, other_matrix.ptr,
				Base::length * sizeof(T)));
	}

	inline const Matrix<CUDAmode, T>& operator=(const Matrix<CUDAmode, T>& input)
	{
		if (!ptr)
		{
			__check_sanity__<CUDAmode> (cuMemFree(ptr));
		}
		Base::length = input.length;
		__check_sanity__<CUDAmode> (cuMemAlloc(&ptr, Base::length * sizeof(T)));
		__check_sanity__<CUDAmode> (cuMemcpyDtoD(ptr, input.ptr, Base::length
				* sizeof(T)));

		return *this;
	}

	inline typename CUDAmode::DevicePtr_Handle& get_ptr()
	{
		return ptr;
	}

	inline ~Matrix()
	{
		/*unsigned int free,total;
		cuMemGetInfo(&free,&total);
		cout<<free<<" "<<total<<endl;*/

		if (ptr)
		{
			__check_sanity__<CUDAmode> (cuMemFree(ptr));
		}
	}

	inline T* to_cpu_ptr() const
	{
		T* ret = new T[Base::length];
		__check_sanity__<CUDAmode> (cuMemcpyDtoH(ret, ptr, Base::length
				* sizeof(T)));
		return ret;
	}

};

template<typename T>
class GLMatrix<CUDAmode, T> : public MatrixBase<T>
{
	typedef typename CUDAmode::Status Status;
	typedef typename CUDAmode::DevicePtr_Handle DevicePtr_Handle;
	typedef typename CUDAmode::Graphics_Handle Graphics_Handle;
	typedef MatrixBase<T> Base;
protected:
	Graphics_Handle vbo;

public:

	GLMatrix(CUDAmode::Context_Handle, CUDAmode::Stream,GLuint ptr)
	{
		__check_sanity__<CUDAmode> (cuGraphicsGLRegisterBuffer(&vbo, ptr,
				CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE));
	}

	typename CUDAmode::DevicePtr_Handle map_ptr()
	{
		typename CUDAmode::DevicePtr_Handle ptr;
		__check_sanity__<CUDAmode> (cuGraphicsMapResources(1, &vbo, 0));
		size_t tmp;// = Base::length * sizeof(T);
		__check_sanity__<CUDAmode> (cuGraphicsResourceGetMappedPointer(&ptr,
				&tmp, vbo));
		Base::length = tmp / sizeof(T);

		return ptr;
	}

	void unmap()
	{
		__check_sanity__<CUDAmode> (cuGraphicsUnmapResources(1, &vbo, 0));
	}
};

#endif
