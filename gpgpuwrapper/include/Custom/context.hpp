/*!
 * \file context.hpp
 * \author vlj
 */




#ifndef CONTEXT_HPP_
#define CONTEXT_HPP_

#include <vector>
#include <map>


/*!
 * This class handles all initialisation/uninitialisation step needed when using GPGPU.
 */
template<class ModeDefinition>
  class Context
  {
    enum
    {
      mode = ModeDefinition::mode
    };
    typedef typename ModeDefinition::Context_Handle Context_Handle;
    typedef typename ModeDefinition::Platform Platform;
    typedef typename ModeDefinition::Device_Handle Device_Handle;
    typedef typename ModeDefinition::Stream Stream;
    typedef typename ModeDefinition::Status Status;
    friend class Kernel<ModeDefinition> ;
  protected:
    Context_Handle cont;
    Platform* platforms;

    Device<ModeDefinition> current_device;
    vector<Device<ModeDefinition> > devices_list;
    map<string, Module<ModeDefinition> > loadedModule;

    static int
    number_of_device();
  public:
    inline const Device_Handle&
    get_dev() const
    {
      return current_device.dev;
    }

    Context();

    /*!
     * Initialise device for computing.
     * This function must be called before any GPGPU computing take place.
     * set_current_device<false> configures device for classic computing
     * set_current_device<true> configures device to use a OpenGL-interoperable environment.
     */
    template<bool isGL>
    void set_current_device(const Device<ModeDefinition>& device);

    /*!
     * Returns a list of usable device on the platform.
     */
    inline const vector<Device<ModeDefinition> >&  get_devices_list()
    {
      return devices_list;
    }

    /*!
     * Eventually loads/builds and returns a reference to a module.
     */
    const Module<ModeDefinition>&  getModule(string filename)
    {
      if (loadedModule.find(filename) == loadedModule.end())
        {
          loadedModule[filename] = Module<ModeDefinition> (filename, cont,
              current_device.dev);
          loadedModule[filename].load();
        }
      return loadedModule[filename];
    }

    /*!
     *  Generate a queue for concurrent query on a device.
     */
    Queue<ModeDefinition>
    genQueue()
    {
      return Queue<ModeDefinition> (cont, current_device.dev);
    }

    ~Context();

    /*!
     *  Create a contigous zone of memory in device memory
     */
    template<typename T>
      inline shared_ptr<Matrix<ModeDefinition, T> >  genMatrix(Queue<ModeDefinition> q, int n, T* tmp = NULL)
      {
        return make_shared<Matrix<ModeDefinition, T> > (cont, q.stream, n, tmp);
      }

    template<typename T>
      inline shared_ptr<GLMatrix<ModeDefinition, T> >  genGLMatrix(Queue<ModeDefinition> q, GLuint vbo)
      {
        return make_shared<GLMatrix<ModeDefinition, T> > (cont, q.stream, vbo);
      }

    /*!
     *  Copy into a new contigous zone of memory in device memory
     */
    template<typename T>
      inline shared_ptr<Matrix<ModeDefinition, T> >  copyMatrix(const shared_ptr<Matrix<ModeDefinition, T> >& input)
      {
        return make_shared<Matrix<ModeDefinition, T> > (*input);
      }

  };

#endif /* CONTEXT_HPP_ */
