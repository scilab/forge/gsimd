/*
 * error.hpp
 *
 *  Created on: 21 juin 2010
 *      Author: vlj
 */

#ifndef ERROR_HPP_
#define ERROR_HPP_

#include <exception>
#include <iostream>
using namespace std;

class Error: public std::exception
{
protected:
	int err_num;
	string err_msg;
public:
	Error(string msg, int id = 0);
	virtual const char* what() const throw ();
	~Error() throw ();
	template<typename ModeDefinition>
	static void treat_error(typename ModeDefinition::Status id);
};

inline Error::Error(string msg, int id) :
	err_num(id), err_msg(msg)
{

}

inline const char* Error::what() const throw ()
{
	return err_msg.c_str();
}

inline Error::~Error() throw ()
{

}

#endif /* ERROR_HPP_ */
