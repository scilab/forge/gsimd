/*
 * matrix.hpp
 *
 *  Created on: 21 juin 2010
 *      Author: vlj
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include <GL/gl.h>

template<typename T>
class MatrixBase
{
protected:
	int length;

public:
	inline
	MatrixBase(int n = 0) :
		length(n)
	{

	}

	inline
	int get_length() const
	{
		return length;
	}

};

template<typename ModeDefinition, typename T>
class Matrix: public MatrixBase<T>
{
  friend class Context<ModeDefinition>;
	friend class Kernel<ModeDefinition> ;
	friend class MatrixFactory<ModeDefinition, T> ;
	typedef typename ModeDefinition::Status Status;
	typedef typename ModeDefinition::DevicePtr_Handle DevicePtr_Handle;
	typedef typename ModeDefinition::Context_Handle Context_Handle;
	typedef typename ModeDefinition::Stream Stream;
	typedef MatrixBase<T> Base;
protected:
	Stream stream;
	Context_Handle cont;
	DevicePtr_Handle ptr;
	Matrix(Context_Handle, Stream, int n, T* tmp = NULL);
    Matrix();
    Matrix(const Matrix& other_matrix);
public:
	const Matrix& operator=(const Matrix& input);
	DevicePtr_Handle& get_ptr();
	~Matrix();
	T* to_cpu_ptr() const;
};

template<typename ModeDefinition, typename T>
class GLMatrix: public MatrixBase<T>
{
	typedef typename ModeDefinition::Context_Handle Context_Handle;
	typedef typename ModeDefinition::Stream Stream;
	typedef typename ModeDefinition::Status Status;
	typedef typename ModeDefinition::DevicePtr_Handle DevicePtr_Handle;
	typedef typename ModeDefinition::Graphics_Handle Graphics_Handle;
	typedef MatrixBase<T> Base;
protected:
	Graphics_Handle vbo;
	Stream stream;
public:
	GLMatrix(Context_Handle, Stream,GLuint ptr);
	DevicePtr_Handle map_ptr();
	void unmap();
};

#endif /* MATRIX_HPP_ */
