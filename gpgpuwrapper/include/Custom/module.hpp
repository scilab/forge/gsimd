/*
 * module.hpp
 *
 *  Created on: 21 juin 2010
 *      Author: vlj
 */

#ifndef MODULE_HPP_
#define MODULE_HPP_

#include <map>
using namespace std;

template<typename ModeDefinition>
class Module
{
	typedef typename ModeDefinition::Status Status;
	typedef typename ModeDefinition::Context_Handle Context_Handle;
	typedef typename ModeDefinition::Module_Handle Module_Handle;
	typedef typename ModeDefinition::Device_Handle Device_Handle;
	friend class Context<ModeDefinition>;
protected:
	Context_Handle cont;
	Device_Handle dev;
	Module_Handle mod;
	bool isloaded;
	string filename;
	map<string, Kernel<ModeDefinition> > storedfonc;

	Module(string f,Context_Handle c=NULL,Device_Handle d=NULL);
	void load();
public:
	Module();
	Module(const Module& input);
	Kernel<ModeDefinition>& getFunction(string kernelname) const;
	~Module();
};


#endif /* MODULE_HPP_ */
