/*
 * queue.hpp
 *
 *  Created on: 30 juin 2010
 *      Author: vlj
 */

#ifndef QUEUE_HPP_
#define QUEUE_HPP_

template<typename ModeDefinition>
class Queue
{
	friend class Context<ModeDefinition>;
	typedef typename ModeDefinition::Context_Handle Context_Handle;
	typedef typename ModeDefinition::Device_Handle Device_Handle;
	typedef typename ModeDefinition::Stream Stream;
	typedef typename ModeDefinition::Status Status;

protected:
	Context_Handle cont;
	Device_Handle dev;
	Queue(Context_Handle,Device_Handle);

public:
	Stream stream;
	Queue();

};

#endif /* QUEUE_HPP_ */
