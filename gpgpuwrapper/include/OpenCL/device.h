/*
 * device.h
 *
 *  Created on: Jun 7, 2010
 *      Author: vlj
 */

#ifndef DEVICE_OPENCL_HPP_
#define DEVICE_OPENCL_HPP_


template<>
inline Device<OPENCLmode>::Device(Device_identifier devid):dev(devid)
{
    char tmp_name[1000];
    clGetDeviceInfo(dev,CL_DEVICE_NAME,sizeof(tmp_name),&tmp_name,NULL);
    name=string(tmp_name);
}

template<>
inline Device<OPENCLmode>::Device()
{

}



#endif /* DEVICE_H_ */
