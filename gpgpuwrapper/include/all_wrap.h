/*!
 * \file all_wrap.h
 *
 * \date 6 juin 2010
 * \author vlj
 */

#ifndef ALL_WRAP_H_
#define ALL_WRAP_H_

enum{
	OpenCL,
	CUDA
};

#include <string>
#include <boost/make_shared.hpp>
using namespace boost;
using namespace std;



class Error;

template<typename ModeDefinition>
class Device;

template<typename ModeDefinition>
class Context;

template<typename ModeDefinition,typename T>
class Matrix;

template<typename ModeDefinition,typename T>
class GLMatrix;

template<typename ModeDefinition>
class Kernel;

template<typename ModeDefinition>
class Queue;

template<typename ModeDefinition>
class Module;

template<class ModeDefinition,typename T>
class MatrixFactory;

template<int mode>
class ModeDefinition;




#include <Custom/error.hpp>

template<typename ModeDefinition>
inline
void __check_sanity__(typename ModeDefinition::Status id)
{
	Error::treat_error<ModeDefinition>(id);
}


#include <Custom/device.hpp>
#include <Custom/module.hpp>
#include <Custom/queue.hpp>
#include <Custom/matrix.hpp>
#include <Custom/kernel.hpp>
#include <Custom/context.hpp>


#include <cuda.h>
#include <GL/gl.h>
#include <cudaGL.h>




template<>
struct ModeDefinition<CUDA>
{
public:
	typedef CUcontext Context_Handle;
	typedef CUresult Status;
	typedef CUdevice Device_Handle;
	typedef CUmodule Module_Handle;
	typedef CUdeviceptr DevicePtr_Handle;
	typedef CUfunction Function_Handle;
	typedef CUstream Stream;
	typedef CUgraphicsResource Graphics_Handle;
	typedef CUdevice Platform;
	typedef int Device_identifier;
	enum{
		mode=CUDA
	};
};

#define CUDAmode ModeDefinition<CUDA>

#include <CUDA/error.hpp>
#include <CUDA/device.hpp>
#include <CUDA/queue.hpp>
#include <CUDA/matrix.hpp>
#include <CUDA/kernel.hpp>
#include <CUDA/module.hpp>
#include <CUDA/context.hpp>




#include <CL/cl.h>
#include <CL/cl_gl.h>

template<>
struct ModeDefinition<OpenCL>
{
public:
	typedef cl_context Context_Handle;
	typedef cl_int Status;
	typedef cl_program Module_Handle;
	typedef cl_device_id Device_Handle;
	typedef cl_mem DevicePtr_Handle;
	typedef cl_kernel Function_Handle;
	typedef cl_command_queue Stream;
	typedef cl_mem Graphics_Handle;
	typedef cl_platform_id Platform;
	typedef cl_device_id  Device_identifier;
	enum{
		mode=OpenCL
	};
};

#define OPENCLmode ModeDefinition<OpenCL>

#include <OpenCL/error.h>
#include <OpenCL/device.h>
#include <OpenCL/queue.h>
#include <OpenCL/matrix.h>
#include <OpenCL/kernel.h>
#include <OpenCL/module.h>
#include <OpenCL/context.h>






#endif /* ALL_WRAP_H_ */
