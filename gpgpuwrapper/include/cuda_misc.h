#define SAFE(command) \
{\
Error::treat_error( command );\
}

using namespace CUDA;
