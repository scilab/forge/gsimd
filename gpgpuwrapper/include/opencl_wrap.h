#include <string>
#include <vector>
#include <exception>
#include <map>
#include <cstdio>
#include <cstring>

#include <CL/cl.h>

#include <all_wrap.h>

#define SAFE(command) \
{\
Error<OpenCL>::treat_error( command );\
}

using namespace std;

#include <OpenCL/error.h>
#include <OpenCL/device.h>
#include <OpenCL/context.h>
#include <OpenCL/matrix.h>
#include <OpenCL/kernel.h>
#include <OpenCL/module.h>

namespace CL
{
typedef Error<OpenCL> Error_t;
typedef Context<OpenCL> Context_t;
typedef Device<OpenCL> Device_t;
typedef Kernel<OpenCL> Kernel_t;
typedef Module<OpenCL> Module_t;
}
