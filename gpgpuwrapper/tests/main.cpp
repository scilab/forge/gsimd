#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <GL/glew.h>

#include <all_wrap.h>

#include <GL/glut.h>
#include <string>
#include <vector>

using namespace std;
using namespace boost::filesystem;

GLuint vbo;
GLuint vbocol;
GLuint vboind;

template<typename ModeDefinition>
class GL_test
{
	typedef shared_ptr<GLMatrix<ModeDefinition, float> > GPUGLMatrix;
protected:
	Context<ModeDefinition> context;
	string ptxpath;
	GPUGLMatrix A;
	Queue<ModeDefinition> queue;
	static boost::function<void()> display_callback;
	static void display_callback_invoke()
	{
		display_callback();
	}
public:

	void display()
	{
		const Module<ModeDefinition>& module = context.getModule(ptxpath);
		Kernel<ModeDefinition>& ker = module.getFunction("glfun");

		ker.pass_argument(A);
		ker.pass_argument(800 * 600 * 4);
		ker.launch(queue,100, 1, 800 * 600 * 4 , 1);
		A->unmap();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexPointer(2, GL_FLOAT, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbocol);
		glColorPointer(4, GL_FLOAT, 0, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboind);

		glDrawElements(GL_QUADS, 599 * 799 * 4, GL_UNSIGNED_INT, 0);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glutSwapBuffers();
		glutPostRedisplay();
	}

	void init()
	{
		float* coords = new float[800 * 600 * 2];
		float* colors = new float[800 * 600 * 4];
		int* index = new int[799 * 599 * 4];
		for (int i = 0; i < 800; ++i)
		{
			for (int j = 0; j < 600; ++j)
			{
				int k = 600 * i + j;
				coords[2 * k] = (j - 300.0) / 300.0;
				coords[2 * k + 1] = (i - 400.0) / 400.0;
				colors[4 * k] = 0.5;
				colors[4 * k + 1] = 0;
				colors[4 * k + 2] = 0;
				colors[4 * k + 3] = 1;
			}
		}

		for (int i = 0; i < 799; i++)
		{
			for (int j = 0; j < 599; j++)
			{
				int ki = 599 * i + j;
				int k = 600 * i + j;

				index[4 * ki] = k;
				index[4 * ki + 1] = k + 1;
				index[4 * ki + 2] = k + 600 + 1;
				index[4 * ki + 3] = k + 600;
			}
		}

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 800 * 600 * 2 * sizeof(float), coords,
				GL_DYNAMIC_DRAW);
		glGenBuffers(1, &vbocol);
		glBindBuffer(GL_ARRAY_BUFFER, vbocol);
		glBufferData(GL_ARRAY_BUFFER, 800 * 600 * 4 * sizeof(float), colors,
				GL_DYNAMIC_DRAW);
		glGenBuffers(1, &vboind);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboind);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 799 * 599 * 4 * sizeof(int),
				index, GL_DYNAMIC_DRAW);

		// default initialization
		glClearColor(0.0, 0.0, 0.0, 1.0);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);

		delete[] coords;
		delete[] colors;
		delete[] index;

		queue=context.genQueue();
		A = context.template genGLMatrix<float>(queue,vbocol);
	}

	GL_test(string filename)
	{

		display_callback = boost::bind(&GL_test::display,this);

		int argc=0;
		char** argv=0;
		int width = 800, height = 600;
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
		glutInitWindowSize(width, height);
		glutCreateWindow("Cuda GL Interop (VBO)");
		glutDisplayFunc(display_callback_invoke);
		glewInit();

		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		context.template set_current_device<true> (context.get_devices_list().at(0));

		ptxpath = initial_path().file_string() + string("/") + filename;

		init();

		glutMainLoop();



	}
};

template<typename ModeDefinition>
boost::function<void()> GL_test<ModeDefinition>::display_callback;


#include <cufft.h>


template<typename ModeDefinition>
class testclass
{
public:

	typedef shared_ptr<Matrix<ModeDefinition, double> > GPUMatrix;

	static double* create_mat(int length)
	{
		double* tmp = new double[length];
		for (int i = 0; i < length; i++)
		{
			tmp[i] = 3.0;
		}
		return tmp;
	}

	static void display(const GPUMatrix& input)
	{
		int length = (*input).get_length();
		double* tmp = (*input).to_cpu_ptr();
		for (int i = 0; i < length; i++)
			cout << tmp[i] << " ";
		delete[] tmp;
		return;
	}

	static void check(const GPUMatrix& input)
	{
		int length = (*input).get_length();
		double* tmp = (*input).to_cpu_ptr();
		for (int i = 0; i < length; i++)
			if(tmp[i]!=6.0)
				throw Error("Wrong Result");
		delete[] tmp;
		return;
	}

	static void classic_test(string testfile)
	{
		Context<ModeDefinition> context;
		context.template set_current_device<false> (
				context.get_devices_list().at(0));
		string ptxpath = initial_path().file_string() + string("/") + testfile;

		const Module<ModeDefinition>& md = context.getModule(ptxpath);
		double* aux = create_mat(16);
		Queue<ModeDefinition> queue = context.genQueue();
		GPUMatrix A = context.genMatrix(queue, 16, aux);
		GPUMatrix B = context.template genMatrix<double> (queue, 16);
		Kernel<ModeDefinition> ker = md.getFunction("add_double");

		ker.pass_argument(B);
		ker.pass_argument(A);
		ker.pass_argument(16);
		ker.launch(queue, 4, 1, 16, 1);
		check(B);
	}

	static void fft_test()
	{
		double* A = create_mat(2*8);
		Context<ModeDefinition> context;
		context.template set_current_device<false>(context.get_devices_list().at(0));

		Queue<ModeDefinition> queue = context.genQueue();
		GPUMatrix gA = context.template genMatrix<double>(queue,2*8,A);
		GPUMatrix gB = context.template genMatrix<double>(queue,2*8);

		cufftDoubleComplex *idata=(cufftDoubleComplex*)(gA->get_ptr());
		cufftDoubleComplex *odata=(cufftDoubleComplex*)(gB->get_ptr());
		cufftHandle plan;

		cufftPlan1d(&plan,8,CUFFT_Z2Z,1);
		cufftExecZ2Z(plan,idata,odata,CUFFT_FORWARD);
		cufftDestroy(plan);


		double B[]={24 , 24 , 0 , 0 , 0 , 0 , 0 , 0 ,
				0 , 0 , 0, 0, 0 , 0 , 0, 0
		};

		for(int k=0;k< 2 * 8; k++)
		{
			if ( (gB->to_cpu_ptr())[k] != B[k])
				throw Error("Wrong result computation");
		}

	}

	static void memory_test()
	{
		double* A=create_mat(4096*4096);
		Context<ModeDefinition> context;
		context.template set_current_device<false>(context.get_devices_list().at(0));

		Queue<ModeDefinition> queue = context.genQueue();
		for(int i=0;i<10;i++)
		{
			GPUMatrix gA = context.template genMatrix<double>(queue,4096*4096,A);
		}

	}

};










int main(int argc, char** argv)
{
	if (argc < 2)
	{
		cout << "No test" << endl;
		return 1;
	}

	string test_to_run=string(argv[1]);

	if(test_to_run=="1")
	{
		cout << "Running CUDA classic test" <<endl;
		testclass<ModeDefinition<CUDA> >::classic_test("../tests/operators.ptx");
	}
	if(test_to_run=="2")
	{
		cout << "Running OpenCL classic test" <<endl;
		testclass<ModeDefinition<OpenCL> >::classic_test("../tests/operator.cl.out");
	}
	if(test_to_run=="3")
	{
		cout << "Running CUDA GL test" <<endl;
		GL_test<ModeDefinition<CUDA> >("../tests/operators.ptx");
	}
	if(test_to_run=="4")
	{
		cout << "Running OpenCL GL test" <<endl;
		GL_test<ModeDefinition<OpenCL> >("../tests/operator.cl.out");
	}

	if(test_to_run=="5")
	{
		cout << "Running cufft interoperability test" << endl;
		testclass<ModeDefinition<CUDA> >::fft_test();
	}

	if(test_to_run=="6")
	{
		cout << "Running CUDA Memory test" << endl;
		testclass<ModeDefinition<CUDA> >::memory_test();
	}

	if(test_to_run=="7")
	{
		cout << "Running OpenCL Memory test" << endl;
		testclass<ModeDefinition<OpenCL> >::memory_test();
	}


	return 0;
}
