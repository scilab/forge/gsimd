__kernel void add_float(__global const float* a, __global const float* b, __global float* c, int iNumElements)
{
    // get index into global data array
    int iGID = get_global_id(0);
    
    // bound check (equivalent to the limit on a 'for' loop for standard/serial C code
    if (iGID >= iNumElements)
    {   
        return; 
    }
    
    // add the vector elements
    c[iGID] = a[iGID] + b[iGID];
}

#pragma OPENCL EXTENSION cl_khr_fp64: enable

__kernel void add_double(__global double* b, __global const double* a, int iNumElements)
{
    // get index into global data array
    int iGID = get_global_id(0);
    
    // bound check (equivalent to the limit on a 'for' loop for standard/serial C code
    if (iGID >= iNumElements)
    {   
        return;
    }
    
    // add the vector elements
    b[iGID] = a[iGID] + a[iGID];
}

__kernel void glfun(__global float* mat,int length)
{
  int iGid = get_global_id(0);
  if( (4*iGid) <length)
  {
    mat[4*iGid]+=1.0/100.0;
  }
}